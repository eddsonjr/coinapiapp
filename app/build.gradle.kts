plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
}

android {
    namespace = "com.edsonjr.mercadobitcoindesafio"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.edsonjr.mercadobitcoindesafio"
        minSdk = 23
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {

        debug {
            buildConfigField("String","BASE_URL","\"https://rest.coinapi.io\"")
            buildConfigField("String","API_KEY","\"86546723-34AC-48B4-A9DA-0AFCFC35B1BA\"")

        }

        release {
            buildConfigField("String","BASE_URL","\"https://rest.coinapi.io\"")
            buildConfigField("String","API_KEY","\"86546723-34AC-48B4-A9DA-0AFCFC35B1BA\"")
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    val lifecycleVersion = "2.6.0"
    val retrofitVersion = "2.9.0"
    val navComponentVersion = "2.5.3"
    val interceptorVersion = "4.9.0"
    val gsonVersion = "2.9.0"
    val gsonRetrofitVersion = "2.9.0"
    val koinVersion = "3.5.3"
    val serializationVersion="1.6.0"


    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")


    //retrofit / logging / Gson
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$interceptorVersion")
    implementation("com.google.code.gson:gson:$gsonVersion")
    implementation("com.squareup.retrofit2:converter-gson:$gsonRetrofitVersion")


    //ViewModel / livedata
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion")

    //Koin (DI)
    implementation("io.insert-koin:koin-core:$koinVersion")
    implementation("io.insert-koin:koin-android:$koinVersion")



    //Navigation Component
    implementation("androidx.navigation:navigation-fragment-ktx:$navComponentVersion")
    implementation("androidx.navigation:navigation-ui-ktx:$navComponentVersion")

}