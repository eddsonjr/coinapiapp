package com.edsonjr.mercadobitcoindesafio.utils

import java.text.SimpleDateFormat
import java.util.Locale

private const val API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'"
private const val DATE_FORMAT = "yyyy/MM/dd"


fun String.convertToCustomDateFormat(): String {
    val inputFormat = SimpleDateFormat(API_DATE_FORMAT, Locale.getDefault())
    val outputFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
    try {
        val date = inputFormat.parse(this)
        return outputFormat.format(date)
    } catch (e: Exception) {
        throw IllegalArgumentException("Invalid date format")
    }
}
