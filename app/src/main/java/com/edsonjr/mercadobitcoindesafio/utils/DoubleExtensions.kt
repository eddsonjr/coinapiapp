package com.edsonjr.mercadobitcoindesafio.utils


fun Double.formatToTwoDecimalPlacesString(): String {
    return String.format("%.2f", this)
}
