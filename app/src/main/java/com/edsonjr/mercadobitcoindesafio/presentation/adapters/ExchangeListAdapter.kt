package com.edsonjr.mercadobitcoindesafio.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.mercadobitcoindesafio.databinding.ItemExchangeListBinding
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange

class ExchangeListAdapter(
    private var listOfExchanges: MutableList<Exchange>?,
    private val clickCallback: (Exchange) -> Unit
):
    RecyclerView.Adapter<ExchangeViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExchangeViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemExchangeListBinding.inflate(layoutInflater,parent,false)
        return ExchangeViewHolder(binding,clickCallback)
    }

    override fun getItemCount(): Int = listOfExchanges?.size ?: 0


    override fun onBindViewHolder(holder: ExchangeViewHolder, position: Int) {
        listOfExchanges?.get(position)?.let { holder.bind(it) }
    }


}