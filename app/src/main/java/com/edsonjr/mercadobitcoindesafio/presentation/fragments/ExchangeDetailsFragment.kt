package com.edsonjr.mercadobitcoindesafio.presentation.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edsonjr.mercadobitcoindesafio.R
import com.edsonjr.mercadobitcoindesafio.databinding.FragmentExchangeDetailsBinding
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange
import com.edsonjr.mercadobitcoindesafio.presentation.fragments.Constants.Companion.EXCHANGE_BUNDLE_ARGS

class ExchangeDetailsFragment : Fragment() {

    private var _binding: FragmentExchangeDetailsBinding? = null
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentExchangeDetailsBinding.inflate(layoutInflater,container,false)
        getArgument()
        return binding?.root
    }

    private fun getArgument() {
        val exchange = arguments?.get(EXCHANGE_BUNDLE_ARGS) as Exchange?
        setupUi(exchange)
    }

    private fun setupUi(exchange: Exchange?) {
        exchange?.let {
            binding?.apply {
                this.exchangeId.text = exchange.exchangeId
                this.exchangeName.text = exchange.name
                this.exchangeWeb.text = exchange.website
                this.exchangeStartDate.text = exchange.dataTradeStart
                this.exchangeEndDate.text = exchange.dataTradeEnd
                this.exchangeVolumePerDay.text = exchange.volume1DayUsd
            }
        }
    }
}