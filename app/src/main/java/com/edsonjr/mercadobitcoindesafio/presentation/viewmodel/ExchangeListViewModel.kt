package com.edsonjr.mercadobitcoindesafio.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edsonjr.mercadobitcoindesafio.data.service.NetworkResult
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange
import com.edsonjr.mercadobitcoindesafio.domain.usecases.ExchangeListUseCase
import kotlinx.coroutines.launch

class ExchangeListViewModel(private val exchangeListUseCase: ExchangeListUseCase): ViewModel() {

    private val TAG = this.javaClass.name

    private val _getExchangeListUIState = MutableLiveData<UIState<List<Exchange>>>()
    val getExchangeListUIState: LiveData<UIState<List<Exchange>>> get() = _getExchangeListUIState



    fun getExchangeList() {
        viewModelScope.launch {
            _getExchangeListUIState.postValue(UIState.Loading)
            val result = exchangeListUseCase.invoke()
            when(result){
                is NetworkResult.Success -> {
                    Log.d(TAG,"SUCCESS: ${result.data}")
                    _getExchangeListUIState.postValue(UIState.Success(result.data))
                }
                is NetworkResult.APIError -> {
                    Log.d(TAG,"API ERROR: ${result.apiException}")



                }
                is NetworkResult.AppFailure -> {
                    Log.d(TAG,"ERROR: ${result.throwable.printStackTrace()}")
                }

            }

        }
    }

}