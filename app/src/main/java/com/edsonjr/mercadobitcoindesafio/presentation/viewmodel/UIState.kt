package com.edsonjr.mercadobitcoindesafio.presentation.viewmodel

import androidx.constraintlayout.motion.utils.ViewState

sealed class UIState<out T> {

    data class Success<T> (val data: T): UIState<T>()
    data object Error: UIState<Nothing>()
    data object Loading: UIState<Nothing>()

}