package com.edsonjr.mercadobitcoindesafio.presentation.adapters

import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.mercadobitcoindesafio.databinding.ItemExchangeListBinding
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange

class ExchangeViewHolder(private val view: ItemExchangeListBinding,
    private val clickCallback: (Exchange) -> Unit): RecyclerView.ViewHolder(view.root) {

        fun bind(item: Exchange) {
            view.exchangeName.text = item.name
            view.exchangeId.text = item.exchangeId
            view.exchangeVolume1dUsd.text = item.volume1DayUsd.toString()

            view.root.setOnClickListener {
                clickCallback(item)
            }
        }
}