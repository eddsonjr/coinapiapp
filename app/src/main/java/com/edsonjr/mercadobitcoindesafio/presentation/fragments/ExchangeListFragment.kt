package com.edsonjr.mercadobitcoindesafio.presentation.fragments

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.edsonjr.mercadobitcoindesafio.R
import com.edsonjr.mercadobitcoindesafio.databinding.FragmentExchangeListBinding
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange
import com.edsonjr.mercadobitcoindesafio.presentation.viewmodel.ExchangeListViewModel
import com.edsonjr.mercadobitcoindesafio.presentation.adapters.ExchangeListAdapter
import com.edsonjr.mercadobitcoindesafio.presentation.fragments.Constants.Companion.EXCHANGE_BUNDLE_ARGS
import com.edsonjr.mercadobitcoindesafio.presentation.viewmodel.UIState
import org.koin.androidx.viewmodel.ext.android.viewModel


class ExchangeListFragment : Fragment() {

    private var _binding: FragmentExchangeListBinding? = null
    private val binding get() = _binding

    private val viewModel by viewModel<ExchangeListViewModel>()
    private lateinit var adapter: ExchangeListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentExchangeListBinding.inflate(layoutInflater,container,false)
        viewModel.getExchangeList()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleExchangeList()
    }

    private fun handleExchangeList() {
        viewModel.getExchangeListUIState.observe(viewLifecycleOwner){ uiState ->
            when(uiState) {
                is UIState.Loading -> { loadingUIState() }
                is UIState.Success -> { setupExchangeAdapter(uiState.data) }
                is UIState.Error -> { }
            }
        }
    }

    private fun setupExchangeAdapter(exchangeList: List<Exchange>) {
        binding?.apply {
            hideLoading()
            adapter = ExchangeListAdapter(exchangeList.toMutableList()) { exchange ->
                val bundle = Bundle()
                bundle.putParcelable(EXCHANGE_BUNDLE_ARGS,exchange)
                findNavController().navigate(R.id.exchangeDetailsFragment,bundle)
                //findNavController().navigate(R.id.exchangeDetailsFragment)
            }

            this.exchangeListRecyclerview.adapter = adapter
        }
    }


    private fun loadingUIState() {
        binding?.let {
            it.loadingContainer.visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        binding?.let {
            it.loadingContainer.visibility = View.GONE
        }
    }


    private fun setupError() {

    }

}