package com.edsonjr.mercadobitcoindesafio.data.service

enum class APIErrorActions {
    WRONG_API_KEY,
    BAD_REQUEST,
    NOT_ENOUGH_PRIVILEGES,
    TOO_MANY_REQUESTS,
    NO_DATA_RETURNED,
    HTTP_INTERNAL_ERROR
}