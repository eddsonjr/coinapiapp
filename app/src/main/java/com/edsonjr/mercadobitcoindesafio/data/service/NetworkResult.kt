package com.edsonjr.mercadobitcoindesafio.data.service

sealed class NetworkResult<out T> {
    data class Success<out T>(val data: T): NetworkResult<T>()
    data class APIError(val apiException: APIException): NetworkResult<Nothing>()
    data class AppFailure(val throwable: Throwable): NetworkResult<Nothing>()
}
