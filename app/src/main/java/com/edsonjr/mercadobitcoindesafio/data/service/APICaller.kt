package com.edsonjr.mercadobitcoindesafio.data.service

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

open class APICaller() {

    suspend fun <T> apiCallExecutor(apiCall: suspend () -> Response<T>): NetworkResult<T> {
        try {
            val response = apiCall()
            if (response.isSuccessful) {
                val responseBody = response.body()
                responseBody?.let {
                    return withContext(Dispatchers.IO) { NetworkResult.Success(responseBody) }
                }
            }
            return withContext(Dispatchers.IO) { checkAPIErrors(response) }
        }catch (e: Exception) {
            return withContext(Dispatchers.IO) { NetworkResult.AppFailure(e) }
        }
    }

    private fun checkAPIErrors(response: Response<*>): NetworkResult.APIError {
        return when(response.code()){
            400 -> NetworkResult.APIError(
                APIException(
                    errorMessage = response.message(),
                    httpStatusCode = 400,
                    errorAction = APIErrorActions.BAD_REQUEST)
            )

            401 -> NetworkResult.APIError(
                APIException(
                    errorMessage = response.message(),
                    httpStatusCode = 401,
                    errorAction = APIErrorActions.WRONG_API_KEY)
            )

            403 -> NetworkResult.APIError(
                APIException(
                    errorMessage = response.message(),
                    httpStatusCode = 403,
                    errorAction = APIErrorActions.NOT_ENOUGH_PRIVILEGES)
            )

            429 -> NetworkResult.APIError(
                APIException(
                    errorMessage = response.message(),
                    httpStatusCode = 429,
                    errorAction = APIErrorActions.TOO_MANY_REQUESTS)
            )

            500 -> NetworkResult.APIError(
                APIException(
                    errorMessage = "Internal server error",
                    httpStatusCode = 500,
                    errorAction = APIErrorActions.HTTP_INTERNAL_ERROR)
            )


            550 -> NetworkResult.APIError(
                APIException(
                    errorMessage = response.message(),
                    httpStatusCode = 550,
                    errorAction = APIErrorActions.NO_DATA_RETURNED)
            )

            else -> NetworkResult.APIError(
                APIException(
                    errorMessage = response.message(),
                    httpStatusCode = response.code()
                )
            )
        }
    }


}