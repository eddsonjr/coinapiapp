package com.edsonjr.mercadobitcoindesafio.data.datasource

import com.edsonjr.mercadobitcoindesafio.data.mapper.ExchangeMapper
import com.edsonjr.mercadobitcoindesafio.data.service.APICaller
import com.edsonjr.mercadobitcoindesafio.data.service.Endpoints
import com.edsonjr.mercadobitcoindesafio.data.service.NetworkResult
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange

class ExchangeListDataSource(
    private val endPoints: Endpoints,
    private val apiCaller: APICaller) {

    suspend fun requestExchangeList(): NetworkResult<List<Exchange>> {
        val requestResult = apiCaller.apiCallExecutor {
            endPoints.getListOfAllExchanges()
        }

        return when (requestResult) {
            is NetworkResult.Success -> {
                val mappedResult = ExchangeMapper.mapExchangeListResponseToEntity(requestResult.data)
                NetworkResult.Success(mappedResult)
            }

            is NetworkResult.APIError -> NetworkResult.APIError(requestResult.apiException)
            is NetworkResult.AppFailure -> NetworkResult.AppFailure(requestResult.throwable)
        }
    }

}