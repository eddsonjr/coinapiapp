package com.edsonjr.mercadobitcoindesafio.data.service

import com.edsonjr.mercadobitcoindesafio.BuildConfig
import com.edsonjr.mercadobitcoindesafio.data.dtos.ExchangeListResponse
import com.edsonjr.mercadobitcoindesafio.data.dtos.ExchangeResponse
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
interface Endpoints {

    companion object {
        private const val V1 = "/v1"
        private const val EXCHANGE_URL = BuildConfig.BASE_URL + V1 + "/exchanges"
    }

    @Headers("X-CoinAPI-Key: " + BuildConfig.API_KEY)
    @GET(EXCHANGE_URL)
    suspend fun getListOfAllExchanges(): Response<List<ExchangeResponse>>

}