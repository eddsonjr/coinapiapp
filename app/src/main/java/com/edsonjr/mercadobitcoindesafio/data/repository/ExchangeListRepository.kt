package com.edsonjr.mercadobitcoindesafio.data.repository

import com.edsonjr.mercadobitcoindesafio.data.service.NetworkResult
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange

interface ExchangeListRepository {
    suspend fun getExchangeList(): NetworkResult<List<Exchange>>

}