package com.edsonjr.mercadobitcoindesafio.data.mapper

import com.edsonjr.mercadobitcoindesafio.data.dtos.ExchangeResponse
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange
import com.edsonjr.mercadobitcoindesafio.utils.convertToCustomDateFormat
import com.edsonjr.mercadobitcoindesafio.utils.formatToTwoDecimalPlacesString

class ExchangeMapper {

    companion object {
        fun mapExchangeListResponseToEntity(response: List<ExchangeResponse>): List<Exchange> {
            val listOfExchanges: MutableList<Exchange> = mutableListOf()
            response.forEach {
                listOfExchanges.add(mapExchangeResponseToEntity(it))
            }
            return listOfExchanges
        }

        private fun mapExchangeResponseToEntity(response: ExchangeResponse): Exchange {
            return Exchange(
                exchangeId = response.exchangeId,
                website = response.website,
                name = response.name,
                dataQuoteStart = response.dataQuoteStart,
                dataQuoteEnd = response.dataQuoteEnd?.convertToCustomDateFormat(),
                dataOrderBookStart = response.dataOrderBookStart?.convertToCustomDateFormat(),
                dataOrderbookEnd = response.dataOrderbookEnd?.convertToCustomDateFormat(),
                dataTradeStart = response.dataTradeStart?.convertToCustomDateFormat(),
                dataTradeEnd = response.dataTradeEnd?.convertToCustomDateFormat(),
                dataSymbolCount =  response.dataSymbolCount,
                volume1HrsUsd = response.volume1HrsUsd?.formatToTwoDecimalPlacesString(),
                volume1DayUsd = response.volume1DayUsd?.formatToTwoDecimalPlacesString(),
                volume1MthUsd = response.volume1MthUsd?.formatToTwoDecimalPlacesString(),
                metricIdList = response.metricIdList
            )

        }
    }
}




