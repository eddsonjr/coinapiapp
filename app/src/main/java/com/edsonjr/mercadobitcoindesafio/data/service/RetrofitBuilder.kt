package com.edsonjr.mercadobitcoindesafio.data.service

import com.edsonjr.mercadobitcoindesafio.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitBuilder {

    private var logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val client = OkHttpClient.Builder()

    fun initRetrofitInstance(): Retrofit {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        client.addInterceptor(logging)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()

        return  Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }


}