package com.edsonjr.mercadobitcoindesafio.data.service

import java.lang.Exception
import java.lang.RuntimeException
import java.net.HttpURLConnection

data class APIException(
    val errorMessage: String,
    val httpStatusCode: Int,
    val errorAction: APIErrorActions? = null
): Exception(errorMessage)