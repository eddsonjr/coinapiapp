package com.edsonjr.mercadobitcoindesafio.data.dtos

data class ExchangeListResponse(
    val exchangeList: List<ExchangeResponse>
)