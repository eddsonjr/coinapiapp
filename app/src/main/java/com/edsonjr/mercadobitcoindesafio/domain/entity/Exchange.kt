package com.edsonjr.mercadobitcoindesafio.domain.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Exchange(

    val exchangeId: String,
    val website: String? = null,
    val name: String? = null,
    val dataQuoteStart: String? = null,
    val dataQuoteEnd: String? = null,
    val dataOrderBookStart: String? = null,
    val dataOrderbookEnd: String? = null,
    val dataTradeStart: String? = null,
    val dataTradeEnd: String? = null,
    val dataSymbolCount: Int? = null,
    val volume1HrsUsd: String? = null,
    val volume1DayUsd: String? = null,
    val volume1MthUsd: String? = null,
    val metricIdList: List<String>?
) : Parcelable