package com.edsonjr.mercadobitcoindesafio.domain.usecases

import com.edsonjr.mercadobitcoindesafio.data.repository.ExchangeListRepository
import com.edsonjr.mercadobitcoindesafio.data.service.NetworkResult
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange

class ExchangeListUseCase(private val repository: ExchangeListRepository) {

    suspend operator fun invoke(): NetworkResult<List<Exchange>> = repository.getExchangeList()
}