package com.edsonjr.mercadobitcoindesafio.domain.repository

import com.edsonjr.mercadobitcoindesafio.data.datasource.ExchangeListDataSource
import com.edsonjr.mercadobitcoindesafio.data.repository.ExchangeListRepository
import com.edsonjr.mercadobitcoindesafio.data.service.NetworkResult
import com.edsonjr.mercadobitcoindesafio.domain.entity.Exchange

class ExchangeListRepositoryImpl(private val dataSource: ExchangeListDataSource): ExchangeListRepository {

    override suspend fun getExchangeList(): NetworkResult<List<Exchange>> = dataSource.requestExchangeList()

}