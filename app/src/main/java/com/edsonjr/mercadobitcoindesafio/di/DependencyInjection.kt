package com.edsonjr.mercadobitcoindesafio.di

import com.edsonjr.mercadobitcoindesafio.data.datasource.ExchangeListDataSource
import com.edsonjr.mercadobitcoindesafio.data.repository.ExchangeListRepository
import com.edsonjr.mercadobitcoindesafio.data.service.APICaller
import com.edsonjr.mercadobitcoindesafio.data.service.Endpoints
import com.edsonjr.mercadobitcoindesafio.data.service.RetrofitBuilder
import com.edsonjr.mercadobitcoindesafio.domain.repository.ExchangeListRepositoryImpl
import com.edsonjr.mercadobitcoindesafio.domain.usecases.ExchangeListUseCase
import com.edsonjr.mercadobitcoindesafio.presentation.viewmodel.ExchangeListViewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object DependencyInjection {

    private val retrofitModule = module {
        single { RetrofitBuilder().initRetrofitInstance() }
        single<Endpoints>{ get<Retrofit>().create(Endpoints::class.java) }
    }

    private val repositoryModule = module {
        single<ExchangeListRepository> { ExchangeListRepositoryImpl(get()) }
    }

    private val dataSourceModule = module {
        single { ExchangeListDataSource(get(),get()) }
    }

    private val viewModelModule = module {
        single { ExchangeListViewModel(get()) }
    }

    private val useCaseModule = module {
        single { ExchangeListUseCase(get()) }
    }

    private val apiCallerModule = module {
        single { APICaller() }
    }


    val appModules = listOf(
        repositoryModule,
        retrofitModule,
        dataSourceModule,
        useCaseModule,
        apiCallerModule,
        viewModelModule
    )

}